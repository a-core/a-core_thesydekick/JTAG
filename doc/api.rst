.. automodule:: jtag.driver
   :members:
   :undoc-members:

.. automodule:: jtag
   :members:
   :special-members: __init__
   :undoc-members:
