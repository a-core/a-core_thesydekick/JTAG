"""
JTAG
----

The ``JTAG`` class represents the JTAG logic in a system. This entity
is used to model the JTAG hardware in rtl simulations. Here it is used
as an ``sv`` model to simulate the JTAG logic defined in the ``chisel``
submodule.


Role of section ``if __name__=="__main__"``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section is used for self-testing by executing the following steps:

1. Instantiate ``JTAG`` as the DUT
2. Instantiate ``jtag_driver``
3. Generate the simulation control sequence using ``jtag_driver`` helpers
4. Connect the entity IOs together, and
5. Invoke the digital simulator

"""

import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *
from rtl import *
from rtl.module import *

import numpy as np

# TODO: link to thesdk package documentation once it's hosted online
class JTAG(rtl, thesdk):
    """
    TheSyDeKick entity for modeling JTAG test logic

    Attributes
    ----------
    IOS : Bundle
        Members of this bundle are the IOs of the entity.  See the documentation of
        ``thesdk`` package. The following members are defined for ``JTAG``:

        ::

            self.IOS.Members['in'] = IO() # pointer for input data
            self.IOS.Members['out'] = IO() # pointer for output data
            self.IOS.Members['jtag_tap_in'] = IO() # JTAG TAP input signals
            self.IOS.Members['jtag_tap_out'] = IO() # JTAG TAP output signals
            self.IOS.Members['control_write'] = IO() # Pointer for control IO for rtl simulations

    model : str
        Hardcoded to ``'sv'`` for rtl simulation.

    """
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self, *arg):
        """
        ``JTAG`` parameters and attributes

        Parameters
        ----------
        *arg : 
            If any arguments are defined, the first one should be the parent instance

        """

        self.print_log(type='I', msg='Initializing %s' %(__name__)) 
        self.proplist = [ 'Rs' ];    # Properties that can be propagated from parent
        self.Rs = 100e6;             # Sampling frequency
        self.model='sv'              # Run RTL simulations by default

        # RTL IO names

        self.parallel_inputs = [
            'tdrio_in_1',
            'tdrio_in_3',
            'tdrio_in_7'
        ]

        self.parallel_outputs = [
            'tdrio_out_1',
            'tdrio_out_3',
            'tdrio_out_5'
        ]

        self.tap_inputs = [
            'tapio_TCK',
            'tapio_TMS',
            'tapio_TDI',
            'tapio_TRSTn',
        ]

        self.tap_outputs = [
            'tapio_TDO_data',
            'tapio_TDO_driven'
        ]

        # IO definitions
        self.IOS=Bundle()

        # sampled IO
        self.IOS.Members['in'] = IO()   # Parallel input data
        self.IOS.Members['out'] = IO()  # Parallel output data

        # event-based IO
        self.IOS.Members['control_write'] = IO() # Simulation control signals

        # JTAG TAP IO
        self.IOS.Members['jtag_tap_in'] = IO()
        self.IOS.Members['jtag_tap_out'] = IO()

        # copy parameter values from the parent based on self.proplist
        if len(arg) >= 1:
            self.parent=arg[0]
            self.copy_propval(self.parent, self.proplist)

        self.init()

    def init(self):
        """
        Re-initialize the structure if the attribute values have been changed after creation.

        """
        pass

    def main(self):
        """
        The main python description of the operation. Unused for this entity.

        """
        pass

    def run(self):
        """
        The default name of the method to be executed. This means: parameters and attributes 
        control what is executed if run method is executed. By this we aim to avoid the need of 
        documenting what is the execution method. It is always ``self.run()``. 

        """

        if self.model in ['sv', 'icarus']:
            # Create iofiles for sampled IO
            _=rtl_iofile(self, name='in', dir='in', iotype='sample', ionames=self.parallel_inputs, datatype='sint')     # parallel inputs
            _=rtl_iofile(self, name='out', dir='out', iotype='sample', ionames=self.parallel_outputs, datatype='sint')  # parallel outputs
            _=rtl_iofile(self, name='jtag_tap_in', dir='in', iotype='sample', ionames=self.tap_inputs, datatype='sint')      # jtag tap inputs
            _=rtl_iofile(self, name='jtag_tap_out', dir='out', iotype='sample', ionames=self.tap_outputs, datatype='sint')   # jtag tap outputs

            # Verilog simulation options
            self.rtlparameters = { 'g_Rs': ('real', self.Rs) }
            self.run_rtl()
        else:
            self.print_log(type='F', msg="'%s' model is not supported")


if __name__=="__main__":
    from jtag import *
    from jtag.controller import controller as sim_controller
    from driver import jtag_driver

    rs=100e6

    dut=JTAG()
    dut.vlogext = '.v'
    dut.model='icarus'
    dut.Rs=rs
    dut.interactive_rtl=True
    #dut.preserve_rtlfiles=True
    #dut.preserve_iofiles=True

    # JTAG driver
    d = jtag_driver()
    d.trstn_polarity = 1    # dut has unsynchronized active high async reset

    # JTAG controller
    controller=sim_controller(dut=dut)
    controller.Rs=rs
    controller.step_time(step=controller.step)
    controller.start_datafeed()

    # Set constant inputs
    # [tdrio_in_1, tdrio_in_3, tdrio_in_7]
    indata = np.array([[0, 0, 51634]])
    dut.IOS.Members['in'].Data = indata

    # Do interesting things with driver
    d.reset_tap()       # reset tap using TRSTn
    d.tms_reset()       # reset tap by asserting TMS and cycling TCK
    d.reset_to_idle()
    
    # toggle one-bit wide TDR register
    d.shift_instruction("00000001")
    d.shift_data("1")
    d.shift_data("0")

    # shift out IDCODE
    d.capture_and_shift_out(icode_str="00000000", tdr_width=32)

    # Capture TDR corresponding to instruction "00000111" and shift it out
    d.tms_reset()
    d.reset_to_idle()
    d.capture_and_shift_out(icode_str="00000111", tdr_width=16)
    
    # Connect IOs together
    dut.IOS.Members['control_write']=controller.IOS.Members['control_write'] # simulation controls
    dut.IOS.Members['jtag_tap_in'].Data=d.get_numpy_array()

    dut.init()
    dut.run()
