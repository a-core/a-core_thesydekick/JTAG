"""
jtag_driver
-----------

The ``jtag_driver`` class is used to generate simulation input excitations for JTAG
test logic. Once created, a ``jtag_driver`` instance can be shared by multiple
user-defined classes for creating further JTAG control helper abstractions on top
of ``jtag_driver`` to control different pieces of test logic over the same JTAG TAP.
Note! jtag_kernel is used as a singleton. Therefore, if you have a python process which
instantiates multiple jtag_kernel's, only one will actually be created. This results in
all jtag waveforms being merged into one. Use ``jtag_driver.clear()`` function to clear
the jtag waveform to avoid it.

"""

import pdb
from rtl import *
from rtl.connector import *
import jtag_kernel

# Entity that controls JTAG TAP input signals
# Used for generating JTAG input excitation waveforms for simulations
class jtag_driver(rtl):
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    @property
    def trstn_polarity(self):
        """
        JTAG asynchronous reset polarity indicated as ``0 = active low`` or ``1 = active high``.
        Default is 0 = active low.
        """
        if not hasattr(self,'_trstn_polarity'):
            self._trstn_polarity=0
        return self._trstn_polarity
    @trstn_polarity.setter
    def trstn_polarity(self, value):
        assert value == 0 or value == 1, "Polarity must be either `0` or `1`"
        self._trstn_polarity=value

    def __init__(self, *arg): 
        self.proplist = [ 'Rs' ];   # properties that can be propagated from parent
        self.Rs = 100e6;            # Default frequency

        self._tdi = 1   # store TDI value from the last cycle

        self.IOS = Bundle() # Create dictionary under jtag_driver
        self.IOS.Members['jtag_tap_in']  = IO()         # get data from kernel as kernel.get_numpy_array()
        self.IOS.Members['jtag_tap_out'] = IO()         # Output can be numpy array from iofile

        self.model = 'py';          # Can be set externally, but is not propagated
        self.par = False            # By default, no parallel processing
        self.queue = []             # By default, no parallel processing

        # Propagate properties
        if len(arg) >= 1:
            parent = arg[0]
            self.copy_propval(parent, self.proplist)
            self.parent = parent

        self.init()

    def init(self):
        self._rtlparameters = {'Rs': self.Rs}
        jtag_kernel.init()

    def clear(self):
        """
        Clear jtag waveform in the singleton python module.
        """
        jtag_kernel.clear()

    # JTAG driver methods

    def jtag_cycle_tms(self, tms):
        """
        Cycle ``TCK`` once while holding ``TMS`` at given value.

        Parameters
        ----------
        tms : int
            TMS value during cycle (``0`` or ``1``)
        """
        # sanity checks
        # assert tms==0 or tms==1
        jtag_kernel.cycle_tms(tms)

    def jtag_cycle_tms_tdi(self, tms, tdi):
        """
        Cycle ``TCK`` once while holding ``TMS`` and ``TDI`` at given values.

        Parameters
        ----------
        tms : int
            TMS value during cycle (``0`` or ``1``)
        tdi : int, None
            TDI value during cycle (``0`` or ``1``)
        """
        # sanity checks
        # assert tms==0 or tms==1
        # assert tdi==0 or tdi==1
        jtag_kernel.cycle_tms_tdi(tms, tdi)

    def reset_tap(self):
        """
        Reset the TAP by asserting ``TRSTn``.

        """
        tck = 0
        tms = 0
        tdi = 0
        # TODO: handle TRSTn polarity
        jtag_kernel.reset_tap()
 
    def tms_reset(self):
        """
        Move TAP controller FSM to *Test-Logic-Reset* state from any other state by
        performing 5 ``TCK`` cycles while holding ``TMS = 1``. Then cycle once more
        to latch any values generated at *Test-Logic-Reset* state.
        
        Takes 6 cycles.

        """
        for i in range(6):
            self.jtag_cycle_tms(1)

    def reset_to_idle(self):
        """
        Move TAP controller FSM from *Test-Logic-Reset* state to *Run-Test/Idle* state.

        Takes 7 cycles.
        """
        self.tms_reset()        # 6 cycles
        self.jtag_cycle_tms(0)  # state 15 = Test-Logic-Reset

    def idle_to_drshift(self):
        """
        Move TAP controller FSM from *Run-Test/Idle* state to *ShiftDR* state.

        Takes 3 cycles.
        """
        self.jtag_cycle_tms(1) # state 12 = Run-Test/Idle
        self.jtag_cycle_tms(0) # state 7 = Select-DR-Scan
        self.jtag_cycle_tms(0) # state 6 = Capture-DR

    def idle_to_irshift(self):
        """
        Move the TAP controller FSM from *Run-Test/Idle* state to *Shift-IR* state.

        Takes 4 cycles.
        """
        self.jtag_cycle_tms(1) # state 12 = Run-Test/Idle
        self.jtag_cycle_tms(1) # state 7 = Select-DR-Scan
        self.jtag_cycle_tms(0) # state 4 = Select-IR-Scan
        self.jtag_cycle_tms(0) # state 14 = Capture-IR

    def drshift_to_idle(self):
        """
        Move the TAP controller FSM from *ShiftDR* state to *Run-Test/Idle* state.

        Takes 2 cycles.
        """
        self.jtag_cycle_tms(1) # state 1 = Exit1-DR
        self.jtag_cycle_tms(0) # state 5 = Update-DR

    def irshift_to_idle(self):
        """
        Move the TAP controller FSM from *Shift-IR* state to *Run-Test/Idle* state.
        
        Takes 2 cycles.
        """
        self.jtag_cycle_tms(1) # state 9 = Exit1-IR
        self.jtag_cycle_tms(0) # state 13 = Update-IR

    def shift(self, tdi_str):
        """
        Shift given data to ``TDI``.

        Parameters
        ----------
        tdi_str : str
            String representation of the binary number to be shifted in.

        Takes ``len(tdi_string)`` cycles.
        """
        if len(tdi_str) > 0:
            tdi_str = tdi_str[::-1]
            for i in range(0, len(tdi_str) - 1):
                self.jtag_cycle_tms_tdi(0, int(tdi_str[i]))
            self.jtag_cycle_tms_tdi(1, int(tdi_str[len(tdi_str) - 1]))

    def shift_instruction(self, tdi_str):
        """
        Shift one insctruction to the instruction register. Assumes that the JTAG
        machine is currently in the *Run-Test/Idle* state.

        Takes ``len(tdi_str) + 6`` cycles.

        Parameters
        ----------
        tdi_str : str
            String representation of the binary number to be shifted in.

        """
        self.idle_to_irshift()  # 4 cycles
        self.shift(tdi_str)     # len(tdi_str) cycles
        self.irshift_to_idle()  # 2 cycles

    def shift_data(self, tdi_str):
        """
        Shift data to the currently selected data register.

        Takes ``len(tdi_str) + 5`` cycles.

        Parameters
        ----------
        tdi_str : str
            String representation of the binary number to be shifted in.

        """
        self.idle_to_drshift()  # 3 cycles
        self.shift(tdi_str)     # len(tdi_str) cycles
        self.drshift_to_idle()  # 2 cycles

    def shift_to_chain(self, icode_str, tdi_str):
        """
        Shift data to a specific data register.

        Takes ``len(icode_str) + len(tdi_str) + 11`` cycles.

        Parameters
        ----------
        tdi_str : str
            String representation of the binary number to be shifted in.
        icode_str : str
            String representation of the binary number to be shifted into the
            Instruction Register, which selects the corresponding Test Data Register.

        """
        self.shift_instruction(icode_str)   # len(icode_str) + 6 cycles
        self.shift_data(tdi_str)            # len(tdi_str) + 5 cycles

    def capture_and_shift_out(self, icode_str, tdr_width):
        """
        Capture parallel input to the currently selected Test Data Register
        and shift out the captured bits to `TDO`.

        Parameters
        ----------
        icode_str : str
            Binary string representation of the Instruction Register corresponding
            to the Test Data Register to be captured.
        tdr_width : int
            Width of the selected Test Data Register in bits.
        """
        # select TDR corresponding to icode_str
        self.shift_instruction(icode_str)

        # capture to TDR, shift out `tdr_width` bytes and move to idle state
        tms_string = "100" + "0"*(tdr_width-1) + "110"
        for c in tms_string:
            self.jtag_cycle_tms(tms=int(c))

    def get_numpy_array(self):
        return jtag_kernel.get_numpy_array()

    def get_remote_bitbang(self):
        return jtag_kernel.get_remote_bitbang()
