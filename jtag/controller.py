# Written by Marko kosunen, Marko.kosunen@aalto.fi 20190530
# The right way to do the unit controls is to write a controller class here
import os

import numpy as np
from thesdk import *
from rtl import *
from rtl.module import *

class controller(rtl):
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self, dut): 
        self.dut=dut
        self.model='sv'
        self.proplist = [ 'Rs' ];    #properties that can be propagated from parent
        self.Rs = 100e6;                   # Sampling frequency
        self.step=int(1/(self.Rs*1e-12))   #Time increment for control
        self.time=0

        # Selected signals controlled with this file with init values
        # These are tuples defining name init value pair
        self.signallist_write = [
            ('initdone', 0)
        ]

        self.IOS=Bundle()
        self.IOS.Members['control_write'] = IO()       # We use this for writing
        _=rtl_iofile(self, name='control_write', dir='in', iotype='event', ionames=['initdone'])

        #Permanent pointer assignment to write io
        self.IOS.Members['control_write'].Data=self.iofile_bundle.Members['control_write']

        # The DUT verilog source location is given by the DUT entity
        # Let's read in the file to have IOs defined
        self.dut_module = verilog_module(file=self.vlogsrc)

        # Define the signal connectors associated with this controller
        # These are signals of tb driving several targets not present in DUT
        self.connectors=verilog_connector_bundle()

        # These are signals not in dut
        self.newsigs_write=['initdone']
        self.newsigs_read=[]
        self.signallist_read=[]

        self.init()

    def init(self):
        """
        Method to re-initialize the structure if the attribute values are changed after creation.

        """
        self._rtlparameters = dict([('Rs',self.Rs)])
        # This gets interesting
        # IO is a file data stucture
        self.define_control()

    def reset_control_sequence(self):
        f=self.iofile_bundle.Members['control_write']
        self.time=0
        f.Data=np.array([])
        f.set_control_data(init=0) # Initialize to zeros at time 0
        self.assign_io()


    # First we start to control Verilog simulations with 
    # This controller. I.e we pass the IOfile definition
    def step_time(self,**kwargs):
        self.time+=kwargs.get('step',self.step)

    def define_control(self):
        # This is a bit complex way of passing the data,
        # But eventually we pass only the data , not the file
        # Definition. File should be created in the testbench
        scansigs_write=[]
        for name, val in self.signallist_write:
            # We manipulate connectors as rtl_iofile operate on those
            if name in self.newsigs_write:
                self.connectors.new(name=name, cls='reg')
            else:
                self.connectors.Members[name]=self.dut_module.io_signals.Members[name]
                self.connectors.Members[name].init=''
            scansigs_write.append(name) 

        f=self.iofile_bundle.Members['control_write']
        #define connectors controlled by this file in order of the list provided 
        f.verilog_connectors=self.connectors.list(names=scansigs_write)

    # Methods to reset and to start datafeed
    def reset(self):
        f=self.iofile_bundle.Members['control_write']
        for name,value in self.signallist_write:
            f.set_control_data(time=self.time,name=name,val=value)
        self.step_time(step=15*self.step)
        for name in [ 'reset' ]:
            f.set_control_data(time=self.time,name=name,val=0)

    def start_datafeed(self):
        f=self.iofile_bundle.Members['control_write']
        for name in [ 'initdone', ]:
            f.set_control_data(time=self.time,name=name,val=1)
        self.step_time()

    def poke(self, name, value):
        """ Drive DUT input signal to a given value

            Parameters
            ----------

            name : str
                Name of the signal to be driven

            value : int
                Drive the signal with this value

        """
        f=self.iofile_bundle.Members['control_write']
        f.set_control_data(time=self.time, name=name, val=value)

