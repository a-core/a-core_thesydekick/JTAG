    ### Test functions ### 
    # TODO: move tests somewhere else
    # Helper for testing idcode 
    def test_shift_to_idreg(self, icode_str="00000000"):
        """ Method to test outputting the devices IDCODE.

        """
        self.shift_to_chain(icode_str, "00000000000000000000000000000000")
    # test BypassChain icode_str = "1111...111"
    def test_shift_to_bypass(self, tdi_str, icode_str="11111111"):
        """ Select and shift data to bypass register.

        """
        self.shift_to_chain(icode_str, tdi_str)
    # test CaptureUpdateChain (TDR with input and output witdh), should be able to Capture & Update
    def test_shift_to_cuchain(self, tdi_str, icode_str="00000001"):
        """ Select and shift data to TDR with CaptureUpdate cells.

        """
        self.shift_to_chain(icode_str, tdi_str)
    # test CaptureChain (TDR with output witdh), should only be able to capture
    def test_shift_to_cchain(self, tdi_str, icode_str="00000100"):
        """ Select and shift data to TDR with Capture cells.

        """
        self.shift_to_chain(icode_str, tdi_str)
    # test UpdateChain (TDR with input width), should only be able to update
    def test_shift_to_uchain(self, tdi_str, icode_str="00000011"):
        """ Select and shift data to TDR with Update cells.

        """
        self.shift_to_chain(icode_str, tdi_str)
    # test ShiftChain (TDR without input or output witdh), should not affect anything
    def test_shift_to_schain(self, tdi_str, icode_str="00000101"):
        """ Select and shift data to TDR with Shift cells.

        """
        self.shift_to_chain(icode_str, tdi_str)
    # test unknown instruction code, IR reg should select IDCODE/BYPASS 
    def test_shift_to_unknown_icode(self, tdi_str, icode_str="00001111"):
        """ Should select bypass register.

        Parameters
        ----------
        tdi_str : str
            String representation of the binary number to be shifted in.
        icode_str : str
            String representation of the corresponding instruction.

        """
        self.shift_to_chain(icode_str, tdi_str)
