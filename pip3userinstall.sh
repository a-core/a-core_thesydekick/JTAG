#!/usr/bin/env bash
#############################################################################
# This is the script to install TheSyDeKick dependencies for the user 
# 
# Created by Marko Kosunen, 2017
#############################################################################
##Function to display help with -h argument and to control 
##The configuration from the command line
help_f()
{
cat << EOF
 PIP3USERINSTALL Release 1.0 (16.01.2020)
 TheSyDeKick dependency installer
 Written by Marko Pikkis Kosunen

 SYNOPSIS
   pip3userinstall.sh [OPTIONS]
 DESCRIPTION
   Installs required Python packages locally to users ~/.local
 OPTIONS
   -u  
       Upgrade also the existing packages. 
       Default: just install the missing ones.
   -h
       Show this help.
EOF
}

THISDIR=$(cd `dirname $0` && pwd)

#Defines TheSDK environment variables
. ${THISDIR}/../../TheSDK.config

PIP="${PYI} -m pip install ${PIPUSEROPT}"
UPGRADE=""
while getopts uh opt
do
  case "$opt" in
    u) UPGRADE="--upgrade";;
    h) help_f; exit 0;;
    \?) help_f; exit 0;;
  esac
  shift
done

#Installs the missing python modules locally with pip3
PACKAGES="\
"

# Install python dependencies through pip
for package in ${PACKAGES}; do
    echo "Installing ${package}"
    ${PIP} ${UPGRADE} ${package}
done

echo "Compiling and installing module jtag_kernel"
echo "${PYI} setup.py install ${PIPUSEROPT}" 
cd jtag_kernel && ${PYI} setup.py install ${PIPUSEROPT} && cd ..

exit 0

