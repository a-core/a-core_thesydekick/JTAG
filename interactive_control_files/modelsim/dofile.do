# simulation controls
add wave -divider "simulation controls"
add wave sim/:tb_JTAG:initdone 

# jtag tap
add wave -divider "JTAG TAP"
add wave -label TCK sim/:tb_JTAG:tapio_TCK
add wave -label TMS sim/:tb_JTAG:tapio_TMS
add wave -label TDI sim/:tb_JTAG:tapio_TDI
add wave -label TRSTn sim/:tb_JTAG:tapio_TRSTn
add wave -label TDO_data sim/:tb_JTAG:tapio_TDO_data
add wave -label TDO_driven sim/:tb_JTAG:tapio_TDO_driven

# test data registers
add wave -divider "test data registers"
add wave -label parallel_in_1 sim/:tb_JTAG:tdrio_in_1 
add wave -label parallel_in_3  sim/:tb_JTAG:tdrio_in_3 
add wave -label parallel_in_7  sim/:tb_JTAG:tdrio_in_7 
add wave -label parallel_out_1 -color "yellow" sim/:tb_JTAG:tdrio_out_1 
add wave -label parallel_out_3  -color "yellow" sim/:tb_JTAG:tdrio_out_3 
add wave -label parallel_out_5  -color "yellow" sim/:tb_JTAG:tdrio_out_5 
radix signal sim/:tb_JTAG:tdrio_in_1  unsigned
radix signal sim/:tb_JTAG:tdrio_in_3  unsigned
radix signal sim/:tb_JTAG:tdrio_in_7  unsigned
radix signal sim/:tb_JTAG:tdrio_out_1 unsigned
radix signal sim/:tb_JTAG:tdrio_out_3 unsigned
radix signal sim/:tb_JTAG:tdrio_out_5 unsigned

# tap controller state decoding
radix define TapControllerState {
    4'b0000 "Exit2-DR"
    4'b0001 "Exit1-DR"
    4'b0010 "Shift-DR"
    4'b0011 "Pause-DR"
    4'b0100 "Select-IR-Scan"
    4'b0101 "Update-DR"
    4'b0110 "Capture-DR"
    4'b0111 "Select-DR-Scan"
    4'b1000 "Exit2-IR"
    4'b1001 "Exit1-IR"
    4'b1010 "Shift-IR"
    4'b1011 "Pause-IR"
    4'b1100 "Run-Test/Idle"
    4'b1101 "Update-IR"
    4'b1110 "Capture-IR"
    4'b1111 "Test-Logic Reset"
}

# outputs to rtl
add wave -divider "outputs to rtl"
add wave -label state sim/:tb_JTAG:io_state
radix signal sim/:tb_JTAG:io_state TapControllerState
add wave -label instruction sim/:tb_JTAG:io_instruction 
add wave -label "tap in reset" sim/:tb_JTAG:io_tapIsInTestLogicReset 

# run simulation
onfinish stop
run -all

# go back to waveform viewer and zoom to fit
view wave
wave zoom full
